from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [

    # freetrail
    url(r'^signup/', views.Authentication().signUp, name='signUp2'),
    url(r'^freetrail/subscribe/', views.Authentication().freeTrailSubscribe, name='freeTrailSubscribe'),
    url(r'^freetrail/signin/', views.Authentication().freeTrailSignIN, name='freeTrailSignIN'),
    url(r'^signin', auth_views.login, name = "login"),
    url(r'^logout', views.Authentication().logout, name = "logout"),
    url(r'^reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset_password',
        views.ResetPasswordRequestView.as_view() , name='reset_password'),
    url(r'^resendrecovery',
        views.ResetPasswordRequestView.as_view() , name='reset_password'),

    # # ----------------
    url(r'^home/', views.Authentication().index, name = "index"),
    # url(r'^home/',include('Project.urls')),
    # # ----------------



]
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.contrib.auth.tokens import default_token_generator
from django.db.models.query_utils import Q
from django.template import loader
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.views.generic import *
from .forms import PasswordResetRequestForm, SetPasswordForm, SignupForm
from django.contrib import messages
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth import authenticate, login, logout
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import Database

class Authentication(object):
    def checkTheUserInFreeTrail(self, user):
        email = user.email
        email_exist = Database.models.UserInformation.objects.filter(email=email).count() > 0
        if email_exist:
            free_trail_user_information = Database.models.UserInformation.objects.get(email=email)
            free_trail_user = free_trail_user_information.user
            free_trail_user.delete()


    def signUp(self, request):
        if request.method == 'POST':
            form = SignupForm(request.POST)
            if form.is_valid():
                user = form.save(commit=True)
                user.is_active = True
                user.save()
                self.checkTheUserInFreeTrail(user)
                userInformation = Database.models.UserInformation(user=user)
                userInformation.save()
                login(request, user)
                return redirect('/')
        else:
            form = SignupForm()
        return render(request, 'Authentication/signUp.html', {'form': form})

    def generateRandomUserName(self):
        count = User.objects.all().count()
        return "psse" + str(count) + "sse"

    def freeTrailSubscribe(self, request):
        if request.method == 'POST':
            email = request.POST['freetrailemailsub']
            email_exist = User.objects.filter(email=email).count() > 0 or Database.models.UserInformation.objects.filter(email = email).count() > 0
            if email_exist:
                context = {'suberr': 'This email is already existed in our Database'}
                return render(request, 'PredModule/index.html', context)
            user = User(username= self.generateRandomUserName())
            # username= email, email = email
            user.is_active = True
            user.save()
            userInformation = Database.models.UserInformation(user=user, email = email, userInFreeTrail = True)
            userInformation.save()
            login(request, user)
            return redirect('/')

    def freeTrailSignIN(self, request):
        if request.method == 'POST':
            email = request.POST['freetrailemailsignin']
            email_exist = Database.models.UserInformation.objects.filter(email = email).count() > 0
            if email_exist:
                user = Database.models.UserInformation.objects.get(email = email).user
                login(request, user)
                return redirect('/')
            else:
                context = {'signinerr': 'Sorry. No such email existed!!!'}
                return render(request, 'PredModule/index.html', context)

    def sendEmail(self, body, reciever, subject):
        fromaddr = "mdrkrashed0719@gmail.com"

        toaddr = str(reciever)
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = subject

        body = body
        msg.attach(MIMEText(body, 'plain'))

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, "01913710569")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()

    def index(self,request):
        print(request.user.email)
        user1 = request.user
        print(user1)
        userInformation = Database.models.UserInformation.objects.get(user=user1)
        print(userInformation.name)
        context={'user':user1,'userInformation':userInformation}
        return render(request, 'Authentication/../templates/PredModule/index.html', context)

    def resendRecovery(self, request):
        if(request.method=="POST"):
            email = request.POST.get('email', False)
            ResetPasswordRequestView().reset_password(request, request.user)

    def logout(self,request):
        logout(request)
        return redirect('/')

class ResetPasswordRequestView(FormView):

    template_name = "registration/password_reset_form.html"
    success_url = '/admin/'
    form_class = PasswordResetRequestForm

    @staticmethod
    def validate_email_address(email):

        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def reset_password(self, user, request):
        c = {
            'email': user.email,
            'domain': request.META['HTTP_HOST'],
            'site_name': 'your site',
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'http',
        }
        subject_template_name = 'registration/password_reset_subject.txt'
        email_template_name = 'registration/password_reset_email.html'
        subject = loader.render_to_string(subject_template_name, c)
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        Authentication().sendEmail(email, user.email,subject)


    def post(self, request, *args, **kwargs):
        data = None
        form = self.form_class(request.POST)
        try:
            if form.is_valid():
                data = form.cleaned_data["email_or_username"]
                # print(data)
            if self.validate_email_address(data) is True:

                associated_users = User.objects.filter(
                    Q(email=data) | Q(username=data))

                if associated_users.exists():

                    for user in associated_users:

                        self.reset_password(user, request)

                    result = self.form_valid(form)

                    messages.success(
                        request, 'An email has been sent to {0}. Please check its inbox to continue reseting password.'.format(data))
                    return render(request, 'registration/EmailSend.html', {'email':data})
                    # return result
                result = self.form_invalid(form)
                messages.error(
                    request, 'No user is associated with this email address')
                return HttpResponse('No user is associated with this email address')
            else:

                associated_users = User.objects.filter(username=data)
                if associated_users.exists():
                    for user in associated_users:
                        self.reset_password(user, request)
                    result = self.form_valid(form)
                    messages.success(
                        request, "Email has been sent to {0}'s email address. Please check its inbox to continue reseting password.".format(data))
                    return result
                result = self.form_invalid(form)
                messages.error(
                    request, 'This username does not exist in the system.')
                return result
        except Exception as e:
            print(e)
        return self.form_invalid(form)


class PasswordResetConfirmView(FormView):
    template_name = "registration/password_reset_confirm.html"
    success_url = '/authentication/signin/'
    form_class = SetPasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password = form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.save()
                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                messages.error(
                    request, 'Password reset has not been unsuccessful.')
                return self.form_invalid(form)
        else:
            messages.error(
                request, 'The reset password link is no longer valid.')
            return self.form_invalid(form)



'''
1. log in korano lagbe
2. 

'''
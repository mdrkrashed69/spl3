from django.apps import AppConfig


class PredictionmoduleConfig(AppConfig):
    name = 'PredictionModule'

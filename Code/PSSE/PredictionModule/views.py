from itertools import combinations

from django.shortcuts import render, redirect
from Database.models import UserInformation, h5models
from keras.models import load_model
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from django.conf import settings
from keras import backend as K
import datetime

from django.core.files import File

def home_page(request):
    user = request.user
    if user.is_authenticated:
        userInformation = UserInformation.objects.get(user = user)
        all_company = h5models.objects.all()
        free_trail = False
        day_remaining = 0
        dayRemainFreeTrail = True
        if userInformation.userInFreeTrail:
            free_trail = True
            date_joined = user.date_joined.date()
            now = datetime.datetime.now().date()
            # time_left = user.date_joined - datetime.datetime
            day_remaining = 7 - (now - date_joined).days
            if day_remaining<=0:
                dayRemainFreeTrail = False


        if request.method == 'POST':
            search_input = request.POST['searchvalue']
            # search_input = str(search_input).lower()
            company_exist = h5models.objects.filter(company__iexact=search_input).count()>0
            print("cm",company_exist)

            # graph = settings.MEDIA_ROOT + '\\' + 'graph.png'
            # f =  open(graph, 'r')
            # graph = File(f)
            # print(graph.url)

            if company_exist:
                company = h5models.objects.get(company__iexact=search_input)
                print(company.dlModel.path)
                ######
                regressor = 0
                regressor = load_model(company.dlModel.path)
                sc = MinMaxScaler(feature_range=(0, 1))
                dataset_test = pd.read_csv(company.test_dataset.path)
                dataset_train = pd.read_csv(company.train_dataset.path)
                # real_stock_price = dataset_test.iloc[:, 4:5].values
                # print(dataset_train)
                # Getting the predicted stock price of 2017
                dataset_total = pd.concat((dataset_train['Opening Price'], dataset_test['Opening Price']), axis=0)
                inputs = dataset_total[len(dataset_total) - len(dataset_test) - 59:].values
                inputs = inputs.reshape(-1, 1)
                inputs = sc.fit_transform(inputs)
                X_test = []
                for i in range(60, 67):
                    X_test.append(inputs[i - 60:i, 0])
                X_test = np.array(X_test)
                X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
                predicted_stock_price = regressor.predict(X_test)
                predicted_stock_price = sc.inverse_transform(predicted_stock_price)

                # Visualising the results
                fig = plt.figure()
                #plt.plot(real_stock_price, color='red', label='Real Google Stock Price')
                plt.plot(predicted_stock_price, color='blue', label='Predicted Stock Price')
                plt.title('Stock Price Prediction')
                plt.xlabel('Time')
                plt.ylabel('Stock Price')
                plt.legend()
                fig.savefig(company.prediction_graph.path)
                K.clear_session()
                ###########

                dataset_train = pd.read_csv(company.trend_dataset.path)
                training_set = dataset_train.iloc[:, 4:5].values
                fig = plt.figure()
                plt.plot(training_set, color='red', label='Real Stock Price Trend')
                # plt.plot(predicted_stock_price, color = 'blue', label = 'Predicted Google Stock Price')
                plt.title('Stock Price Trend')
                plt.xlabel('Day')
                plt.ylabel('Stock Price')
                plt.legend()
                # plt.show()
                fig.savefig(company.price_trend_graph.path)

                ###########

                ######
                context = {'user': user,
                           'found': True,
                           'company': company,
                           'all_company': all_company,
                           'freeTrail': free_trail,
                           'dayRemain': day_remaining,
                           'dayRemainFreeTrail': dayRemainFreeTrail,
                           }

            else:
                context = {'user': user,
                           'found': False,
                           'all_company': all_company,
                           'freeTrail': free_trail,
                           'dayRemain': day_remaining,
                           'dayRemainFreeTrail': dayRemainFreeTrail,
                           }

            return render(request, 'PredModule/companyDetails.html', context)
        else:

            context = {'user': user,
                       'userInformation': userInformation,
                       'all_company': all_company,
                       'freeTrail': free_trail,
                       'dayRemain': day_remaining,
                       'dayRemainFreeTrail': dayRemainFreeTrail,
                       }
            return render(request, 'PredModule/prediction.html',context)
    else:
        return render(request, 'PredModule/index.html')
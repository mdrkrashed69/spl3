from django.db import models
from django.contrib.auth.models import User

def user_directory_path(instance, filename):
    return '{0}/{1}'.format(instance.user.username, filename)

def model_path(instance, filename):
    return '{0}/{1}/{2}'.format(instance.company, 'models', filename)

def csv_path(instance, filename):
    return '{0}/{1}/{2}'.format(instance.company, 'csv', filename)

class UserInformation(models.Model):

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True
    )
    email = models.CharField(default="None", max_length=200)
    userInFreeTrail = models.BooleanField(default=False)
    name = models.CharField(default="None", max_length=200)
    biography = models.TextField(default="None",)
    profilePicture = models.FileField(default='../media/defaultProfilePic.png/',upload_to=user_directory_path,)

    def __str__(self):
        return self.name

class h5models(models.Model):

    company = models.CharField(max_length = 200)
    company_details = models.TextField(default="About the company")
    market_capitalization = models.TextField()
    share_holders_equity = models.TextField()
    book_value_per_share = models.TextField()
    dlModel = models.FileField(upload_to=model_path,)
    test_dataset = models.FileField(upload_to=csv_path,)
    train_dataset = models.FileField(upload_to=csv_path,)
    trend_dataset = models.FileField(upload_to=csv_path,)
    prediction_graph = models.FileField(default='../media/graph.png/')
    price_trend_graph = models.FileField(default='../media/price_trend.png/')

    def __str__(self):
        return self.company

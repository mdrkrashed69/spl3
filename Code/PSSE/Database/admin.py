from django.contrib import admin
from .models import UserInformation, h5models
admin.site.register(UserInformation)
admin.site.register(h5models)
